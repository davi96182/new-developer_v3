import unittest


class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('hello'.upper(), 'HELLO')


if __name__ == '__main__':
    unittest.main()
