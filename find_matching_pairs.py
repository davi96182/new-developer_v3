def find_matching_pair(numbers, target_sum):
    """
    Finds a matching pair of numbers in the given list that add up to the target sum.
    Returns the pair of numbers as a tuple, or None if no such pair exists.
    Assumes that the list is sorted in ascending order.
    """
    left_index = 0
    right_index = len(numbers) - 1
    while left_index < right_index:
        current_sum = numbers[left_index] + numbers[right_index]
        if current_sum == target_sum:
            return (numbers[left_index], numbers[right_index])
        elif current_sum < target_sum:
            left_index += 1
        else:
            right_index -= 1
    return None


# Example usage:
numbers = [2, 3, 6, 7]
target_sum = 9
matching_pair = find_matching_pair(numbers, target_sum)
print(matching_pair)  # (3, 6)
