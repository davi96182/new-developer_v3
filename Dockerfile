FROM python:3.9-slim-buster

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN pylint *.py
RUN python -m unittest discover

CMD ["python", "find_matching_pairs.py"]